import { outputAst } from '@angular/compiler';
import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-hijo',
  templateUrl: './hijo.component.html',
  styleUrls: ['./hijo.component.css']
})
export class HijoComponent implements OnInit {



  constructor() { }
  
  mensaje1: string = 'Primer mensaje desde el hijo';
  mensaje2: string = 'Segundo mensaje desde el hijo';
  mensaje3: string = 'Tercer mensaje desde el hijo';
  mensaje4: string = 'Cuarto mensaje desde el hijo';
  mensaje5: string = 'Quinto mensaje desde el hijo'

  @Output() Message1 = new EventEmitter<string>();
  @Output() Message2 = new EventEmitter<string>();
  @Output() Message3 = new EventEmitter<string>();
  @Output() Message4 = new EventEmitter<string>();
  @Output() Message5 = new EventEmitter<string>();

  ngOnInit(): void {
    this.Message1.emit(this.mensaje1);
    this.Message2.emit(this.mensaje2)
    this.Message3.emit(this.mensaje3)
    this.Message4.emit(this.mensaje4)
    this.Message5.emit(this.mensaje5)
  }

}
